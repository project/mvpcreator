Behat tests
===========

Setup
-----

 1. Install Composer

    php -r "eval('?>'.file_get_contents('https://getcomposer.org/installer'));"
 
 2. Install Behat and dependencies

    php composer.phar install

 2. Copy behat.yml.example to behat.yml and modify

    mv behat.template.yml behat.yml
 
 3. Run Behat and examine test results!
 
    bin/behat

