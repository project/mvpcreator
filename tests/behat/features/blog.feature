Feature: Publish articles on the blog
  In order to communicate with customers and potential customers
  As a member of the marketing team
  I need to be able to publish on the blog

  # TODO: How can we test the empty blog listing page?

  @api
  Scenario: View the "Create blog post" form as an editor
    Given I am logged in as a user with the "editor" role
    When I follow "Add content"
      And I follow "Blog post"
    # TODO: Editor should get the comment settings and path alias stuff too!
    Then I should be on "/node/add/mvpcreator-blog"
      And I should see "Title" in the "BURR Flipped Content" region
      And I should see "Permalink" in the "BURR Flipped Content" region
      And I should see "Body" in the "BURR Flipped Content" region
      And I should see "Comment settings" in the "BURR Flipped Content" region
    Then I should see the heading "Featured image" in the "BURR Flipped Sidebar" region
      And I should see the heading "Content category" in the "BURR Flipped Sidebar" region
      And I should see "Tags" in the "BURR Flipped Sidebar" region
      And I should see the heading "Publishing options" in the "BURR Flipped Sidebar" region
      And I should see "Feature content" in the "BURR Flipped Sidebar" region
      And I should not see "Create new revision" in the "BURR Flipped Sidebar" region
      And I should see "Revision log message" in the "BURR Flipped Sidebar" region
      And I should not see "Authoring information" in the "BURR Flipped Sidebar" region

  @api
  Scenario: View the "Create blog post" form as an administrator
    Given I am logged in as a user with the "administrator" role
    When I follow "Add content"
      And I follow "Blog post"
    Then I should be on "/node/add/mvpcreator-blog"
      And I should see "Title" in the "BURR Flipped Content" region
      And I should see "Permalink" in the "BURR Flipped Content" region
      And I should see "Body" in the "BURR Flipped Content" region
      And I should see "Comment settings" in the "BURR Flipped Content" region
    Then I should see the heading "Featured image" in the "BURR Flipped Sidebar" region
      And I should see the heading "Content category" in the "BURR Flipped Sidebar" region
      And I should see "Tags" in the "BURR Flipped Sidebar" region
      And I should see the heading "Publishing options" in the "BURR Flipped Sidebar" region
      And I should see "Feature content" in the "BURR Flipped Sidebar" region
      And I should see "Create new revision" in the "BURR Flipped Sidebar" region
      And I should see "Revision log message" in the "BURR Flipped Sidebar" region
      And I should see "Authoring information" in the "BURR Flipped Sidebar" region

   @api
   Scenario: Publish blog post
    Given I am logged in as a user with the "editor" role
    When I visit "/node/add/mvpcreator-blog"
      # TODO: add an image!
      And I fill in the following:
        | Title                | Welcome to the blog!        |
        | body[und][0][value]  | <p>This is a blog post.</p> |
        | Tags                 | one, two                    |
        | Revision log message | Initial version.            |
      # TODO: We need to make sure the "culture" taxonomy is available.
      And I check the box "culture"
      And I press "Publish"
    Then the "h1" element should contain "Welcome to the blog!"
      And I should see "This is a blog post."
      And I should see the link "culture"
      And I should see the link "one"
      And I should see the link "two"
        
   @api
   Scenario: Save a draft blog post
    Given I am logged in as a user with the "editor" role
    When I visit "/node/add/mvpcreator-blog"
      And I fill in the following:
        | Title                | Welcome to the blog!        |
        | body[und][0][value]  | <p>This is a blog post.</p> |
      And I press "Save as draft"
    Then the "h1" element should contain "Welcome to the blog!"
    When I click "Edit"
    Then I should see "Publish"

  @api
  Scenario: See comment form as an anonymous user
    Given I am an anonymous user
      And I am viewing a "mvpcreator_blog" node with the title "Very interesting article"
    Then the "h1" element should contain "Very interesting article"
      And I should see "Add comment"
      And I should see the link "Gravatar"
      And I should see "it will be used to display your avatar"
    When I fill in the following:
      | Your name | Test Guy            |
      | E-mail    | test@example.com    |
      | Homepage  | http://example.com  |
      | Comment   | Here is my comment. |
      # Issue #2104299: Previewing comment will use all available memory.
      And I press "Preview"
    Then I should see "Here is my comment."

