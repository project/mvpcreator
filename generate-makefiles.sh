#!/bin/bash

# Here are a number of themes and modules, that we're going to exclude
# from sites-all.make, because they are still too unstable to include
# in a "shared platform" of the best contrib modules.
UNSTABLE="radix navbar"

if [ -z "$DRUSH" ]; then
  DRUSH=`which drush5`

  if [ -z "$DRUSH" ]; then
  	DRUSH=`which drush`
  fi
fi

$DRUSH makefile-combine --yes makefiles/mvpcreator.make drupal-org.make
$DRUSH makefile-combine --yes makefiles/sites-all.make  sites-all.make

# Remove the 'contrib' subdirs for sites-all.make
sed -i '/^projects\[.\+\]\[subdir\] = "contrib"/d' sites-all.make

# Remove projects that we consider to be too unstable
for project in $UNSTABLE; do
  sed -i "/^projects\[$project\]/d" sites-all.make
done

