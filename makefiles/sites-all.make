api = 2
core = 7.x

; This just contains the contrib modules, themes and libraries that we want
; in sites/all/* for all install profiles.

includes[] = includes/contrib-modules.make
includes[] = includes/contrib-themes.make
includes[] = includes/libraries.make
includes[] = includes/panopoly-dependencies.make

