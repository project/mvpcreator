api = 2
core = 7.x

; All our includes contrib modules

includes[] = includes/contrib-modules.make
includes[] = includes/contrib-themes.make
includes[] = includes/libraries.make
includes[] = includes/panopoly.make

; Our apps (included directly in the profile for now)

projects[mvpcreator_blog][version] = 1.x-dev
projects[mvpcreator_blog][subdir] = mvpcreator
projects[mvpcreator_blog][type] = module
projects[mvpcreator_blog][download][type] = git
projects[mvpcreator_blog][download][revision] = 138218f
projects[mvpcreator_blog][download][branch] = 7.x-1.x

