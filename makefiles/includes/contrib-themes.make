
core = 7.x
api = 2

projects[adaptivetheme][version] = 3.2

projects[omega][version] = 3.1

projects[radix][download][type] = git
projects[radix][download][url] = http://git.drupal.org/project/radix.git
projects[radix][download][branch] = 7.x-2.x
projects[radix][download][revision] = d92c98b07cbda965f2c4ef8f9f37e241ad0535b5

projects[rubik][version] = 4.3

projects[tao][version] = 3.1

projects[zen][version] = 5.6

