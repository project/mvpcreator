api = 2
core = 7.x

projects[addressfield][version] = 1.2
projects[addressfield][subdir] = contrib

projects[advanced_help][version] = 1.3
projects[advanced_help][subdir] = contrib

projects[backup_migrate][version] = 2.8
projects[backup_migrate][subdir] = contrib

projects[better_formats][version] = 1.0-beta1
projects[better_formats][subdir] = contrib

projects[captcha][version] = 1.3
projects[captcha][subdir] = contrib

projects[calendar][version] = 3.5
projects[calendar][subdir] = contrib

projects[ckeditor][version] = 1.16
projects[ckeditor][subdir] = contrib

projects[computed_field][version] = 1.0
projects[computed_field][subdir] = contrib

projects[colorbox][version] = 2.10
projects[colorbox][subdir] = contrib

projects[context][version] = 3.6
projects[context][subdir] = contrib

projects[delta][version] = 3.0-beta11
projects[delta][subdir] = contrib

projects[diff][version] = 3.2
projects[diff][subdir] = contrib

projects[email][version] = 1.3
projects[email][subdir] = contrib

projects[extlink][version] = 1.18
projects[extlink][subdir] = contrib

projects[feeds][version] = 2.0-beta1
projects[feeds][subdir] = contrib

projects[feeds_tamper][version] = 1.1
projects[feeds_tamper][subdir] = contrib

projects[fontyourface][version] = 2.8
projects[fontyourface][subdir] = contrib

projects[getclicky][version] = 1.x-dev
projects[getclicky][subdir] = contrib
projects[getclicky][type] = module
projects[getclicky][download][type] = git
projects[getclicky][download][revision] = 88da32555d0acb5e65acba08ea00fdee4f8af986
projects[getclicky][download][branch] = 7.x-1.x

projects[google_analytics][version] = 2.3
projects[google_analytics][subdir] = contrib

projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib

projects[i18n][version] = 1.13
projects[i18n][subdir] = contrib

projects[imce][version] = 1.9
projects[imce][subdir] = contrib

projects[imce_wysiwyg][version] = 1.0
projects[imce_wysiwyg][subdir] = contrib

projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][subdir] = contrib

projects[l10n_update][version] = 1.1
projects[l10n_update][subdir] = contrib

projects[lightbox2][version] = 1.0-beta1
projects[lightbox2][subdir] = contrib

projects[mailchimp][version] = 2.12
projects[mailchimp][subdir] = contrib

projects[mailsystem][version] = 2.34
projects[mailsystem][subdir] = contrib

projects[mandrill][version] = 1.6
projects[mandrill][subdir] = contrib

projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = contrib

projects[metatag][version] = 1.7
projects[metatag][subdir] = contrib

projects[mixpanel][version] = 1.2
projects[mixpanel][subdir] = contrib

projects[mollom][version] = 2.15
projects[mollom][subdir] = contrib

projects[name][version] = 1.10
projects[name][subdir] = contrib

projects[omega_tools][version] = 3.0-rc4
projects[omega_tools][subdir] = contrib

projects[page_title][version] = 2.7
projects[page_title][subdir] = contrib

projects[panels_tabs][version] = 1.x-dev
projects[panels_tabs][subdir] = contrib
projects[panels_tabs][type] = module
projects[panels_tabs][download][type] = git
projects[panels_tabs][download][revision] = 03b90c5cca3720170e9a8cf683352eb21855684d
projects[panels_tabs][download][branch] = 7.x-1.x

projects[panels_extra_styles][version] = 1.1
projects[panels_extra_styles][subdir] = contrib

projects[panels_style_table][version] = 1.x-dev
projects[panels_style_table][subdir] = contrib
projects[panels_style_table][type] = module
projects[panels_style_table][download][type] = git
projects[panels_style_table][download][revision] = b712d421b8f290e39680ea55a1d404238fd284b4
projects[panels_style_table][download][branch] = 7.x-1.x

projects[phone][version] = 1.x-dev
projects[phone][subdir] = contrib
projects[phone][type] = module
projects[phone][download][type] = git
projects[phone][download][revision] = 173dd71fc755c61ed7757f3ea5d7e12ce970e5d9
projects[phone][download][branch] = 7.x-1.x
projects[phone][patch][2029047] = "http://drupal.org/files/phone-feeds-validation-2029047.patch"

projects[postmark][version] = 1.2
projects[postmark][subdir] = contrib

projects[remember_me][version] = 1.1
projects[remember_me][subdir] = contrib

projects[relation][version] = 1.0
projects[relation][subdir] = contrib

projects[select_or_other][version] = 2.22
projects[select_or_other][subdir] = contrib

projects[signup_share_track][version] = 1.0-alpha4
projects[signup_share_track][subdir] = contrib

projects[spambot][version] = 1.4
projects[spambot][subdir] = contrib

projects[title][version] = 1.0-alpha9
projects[title][subdir] = contrib

projects[transliteration][version] = 3.2
projects[transliteration][subdir] = contrib

projects[typogrify][version] = 1.0-rc10
projects[typogrify][subdir] = contrib

projects[rules][version] = 2.9
projects[rules][subdir] = contrib

projects[uuid_features][version] = 1.0-alpha4
projects[uuid_features][subdir] = contrib

projects[variable][version] = 2.5
projects[variable][subdir] = contrib

projects[views_slideshow][version] = 3.1
projects[views_slideshow][subdir] = contrib

projects[views_slideshow_slider][version] = 3.0
projects[views_slideshow_slider][subdir] = contrib

projects[webform][version] = 4.12
projects[webform][subdir] = contrib

projects[xmlsitemap][version] = 2.3
projects[xmlsitemap][subdir] = contrib

