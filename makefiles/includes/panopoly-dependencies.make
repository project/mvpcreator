
core = 7.x
api = 2

includes[] = ../../modules/panopoly/panopoly_admin/panopoly_admin.make
includes[] = ../../modules/panopoly/panopoly_core/panopoly_core.make
includes[] = ../../modules/panopoly/panopoly_images/panopoly_images.make
;includes[] = ../../modules/panopoly/panopoly_magic/panopoly_magic.make
;includes[] = ../../modules/panopoly/panopoly_pages/panopoly_pages.make
includes[] = ../../modules/panopoly/panopoly_search/panopoly_search.make
includes[] = ../../modules/panopoly/panopoly_theme/panopoly_theme.make
includes[] = ../../modules/panopoly/panopoly_users/panopoly_users.make
includes[] = ../../modules/panopoly/panopoly_widgets/panopoly_widgets.make
includes[] = ../../modules/panopoly/panopoly_wysiwyg/panopoly_wysiwyg.make

