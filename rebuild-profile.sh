#!/bin/bash

if [ -z "$DRUSH" ]; then
  DRUSH=`which drush5`

  if [ -z "$DRUSH" ]; then
  	DRUSH=`which drush`
  fi
fi

$DRUSH make --yes --no-core --contrib-destination=. makefiles/mvpcreator.make $@

